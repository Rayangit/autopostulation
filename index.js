const cheerio = require('cheerio'),
  axios = require('axios'),
 FormData = require('form-data');

const postNodeDiscord = async (message) => {
  var finalString = `\n${message.title}\n\n${message.description.replace('Voir plus', '\n')}\n${message.link}`
  var formData = new FormData()

  formData.append('content', finalString)
  var config = {
    method: 'post',
    url: 'https://discord.com/api/webhooks/794740573620797461/Qp25j8_qr5j43S1MOeUu5nVS8KArDalcDrZrabJBFiTvOzMXSbACjNFkzPfhGTy0Ealv',
    headers: {
      ...formData.getHeaders()
    },
    data : formData
  };
  axios(config)
    .then(function (response) {
        console.log(response);
      }
    )
    .catch(function (error) {
      console.log(error);
    });
}

const postPythonDiscord = async (message) => {
  var finalString = `\n${message.title}\n\n${message.description.replace('Voir plus', '\n')}\n${message.link}`
  var formData = new FormData()

  formData.append('content', finalString)
  var config = {
    method: 'post',
    url: 'https://discord.com/api/webhooks/794741721082757180/QX0g6tyTN4MI1elfJe6y_VE20bphs0YosPlUJdDzorNyr2Lwu7L9XZpJON2T3SH9sTR5',
    headers: {
      ...formData.getHeaders()
    },
    data : formData
  };
  axios(config)
    .then(function (response) {
        console.log(response);
      }
    )
    .catch(function (error) {
      console.log(error);
    });
}

const postDevOpsDiscord = async (message) => {
  var finalString = `\n${message.title}\n\n${message.description.replace('Voir plus', '\n')}\n${message.link}`
  var formData = new FormData()

  formData.append('content', finalString)
  var config = {
    method: 'post',
    url: 'https://discord.com/api/webhooks/794739310142029885/7EI9Rr6eUOOTAYXwb5aUoMcvjp9P0qoHv9miDpOjjrOfmT5EUySe0mAJqQ2wkJS5CANt',
    headers: {
      ...formData.getHeaders()
    },
    data : formData
  };
  axios(config)
    .then(function (response) {
        console.log(response);
      }
    )
    .catch(function (error) {
      console.log(error);
    });
}

const main = async () => {
    const baseUrl = 'https://www.freelance-info.fr',
    url = `https://www.freelance-info.fr/missions?page=`,
    getDate = new Date();
  var todaysMissions = []
  var isLastPage = false
  var page = 1
  while (!isLastPage) {
    const missionsPage = await axios.get(url + page)
      .then((response) => {
        var retTab = []
        let $ = cheerio.load(response.data);
        $('.roffre').each(function (i, e) {
          const ret = {
            title: String,
            date: Number,
            description: String,
            link: String
          }
          ret.title = $(e).find('.rtitre').text().replace(/\s\s+/g, '');
          ret.description = $(e).find('.text-justify').text().replace(/\s\s+/g, '');
          ret.date = $(e).find('.textgrisfonce9').text().replace(/\s\s+/g, '').substring(0, 2)
          ret.link = baseUrl + $(this).find('#titre-mission a').attr('href');
          const today = String(getDate.getDate()).padStart(2, '0')
          if (ret.date === today) {
            if (ret.title.toLowerCase().includes('devops')
              || ret.description.toLowerCase().includes('devops')
              || ret.title.toLowerCase().includes('front')
              || ret.description.toLowerCase().includes('front')
              || ret.title.toLowerCase().includes('python')
              || ret.description.toLowerCase().includes('python')
              || ret.title.toLowerCase().includes('react')
              || ret.description.toLowerCase().includes('react')
              || ret.title.toLowerCase().includes('node')
              || ret.description.toLowerCase().includes('node')
              || ret.title.toLowerCase().includes('vue')
              || ret.description.toLowerCase().includes('vue')
            )
              retTab.push(ret)
          } else {
            isLastPage = true
            return false
          }
        })
        return retTab
      }).catch(function (e) {
        console.log(e);
      })
    todaysMissions = [...todaysMissions, ...missionsPage]
    page++
  }
  todaysMissions.forEach(mission => {
    if (mission.title.toLowerCase().includes('devops') || mission.description.toLowerCase().includes('devops'))
      postDevOpsDiscord(mission)
    else if (mission.title.toLowerCase().includes('python') || mission.description.toLowerCase().includes('python'))
      postDevOpsDiscord(mission)
    else if (mission.title.toLowerCase().includes('react')
      || mission.description.toLowerCase().includes('react')
      || mission.description.toLowerCase().includes('vue')
      || mission.description.toLowerCase().includes('vue')
      || mission.description.toLowerCase().includes('node')
      || mission.description.toLowerCase().includes('node'))
      postNodeDiscord(mission)
  })
};

main()
